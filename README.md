# LangChain with Mindformers

## 0.简介

MindSpore Transformers（简称：MindFormers）是昇思MindSpore全场景AI框架中的开源大模型训练套件，其目标是构建一个大模型训练、微调、评估、推理、部署的全流程开发套件：提供业内主流的Transformer类预训练模型和SOTA下游任务应用，涵盖丰富的并行特性。期望帮助用户轻松的实现大模型训练和创新研发。

LangChain旨在帮助开发人员使用语言模型构建端到端的应用程序。LangChain提供了一套工具、组件和接口，可简化创建由大语言模型提供支持的应用程序的过程。LangChain 可以轻松管理与语言模型的交互，将多个组件链接在一起，并集成额外的资源，例如API和数据库。

通过整合，LangChain框架可以利用MindFormers套件模型和API的能力，提供更高水平的语言处理功能，从而使应用程序能够更好地理解和生成自然语言。

LangChain框架的开发人员可以直接使用MindFormers套件的部分模型和API，而不需要单独开发和集成这些功能。这大大减少了开发工作量，加快了应用程序的开发速度。

开发人员可以将大语言模型作为组件之一，并与其他组件进行无缝集成。这使得应用程序可以充分利用大语言模型的语言处理能力，构建出更丰富、多样化的应用。

开发人员可以借助LangChain框架实现已有知识库的向量化与管理，并通过该框架实现知识库与现有大语言模型的连接，在大预言模型进行文本生成前通过知识库匹配为大语言模型提供相关知识，进而有效提升大语言模型的生成效率，并可通过知识库更新，为大语言模型提供一定的实时更新能力，避免大语言模型的重复训练。

## 1. 仓库介绍

`LangChain with Mindformers` 基于 `mindformers` 与 `LangChain` 实现，主要涉及的文件有：

1. mindformers本地模型与LangChain框架组合使用：`glm_with_langchain.py`

2. 通过LangChain框架管理向量库并基于向量库对mindformers本地模型问答进行优化：`chatbot.py`

3. 基于mindformers的本地模型具体实现：`mindformers/models/glm`

   ```bash
   glm
       ├── __init__.py
       ├── attention.py            # 自注意力
       ├── chatglm_6b_tokenizer.py # tokenizer
       ├── glm_config.py           # 模型配置项
       ├── glm.py                  # 模型实现
       └── layers.py               # glm 层定义
   ```

4. 需调用模组：`scrs/`

   ```bash
   scrs
       ├── chinese_test_spliter.py     # 中文文本切分工具
       ├── ErnieModel.py               # ErnieModel1.0模型用于文本向量化
       ├── msembding.py                # 基于mindspore实现的对接LangChain框架的文本向量化支持文件
       ├── msTransformers.py           # 基于mindspore实现的对接LangChain框架的文本向量化模型文件
       └── mspipeline.py               # 基于mindspore实现的对接LangChain框架的LLM模型支持文件
      
   ```

## 2. 环境准备

- 硬件：Ascend 910A
- MindSpore：2.0.0rc1
- CANN版本：6.3.RC1.alpha003
- 驱动：23.0.rc1

宁波AICC直接注册：swr.cn-east-321.nbaicc.com/nbaicc_pub/ms20rc1_py39_cann63:2023062601 

使用的环境为`mindspore_py39`，可使用如下命令进行环境切换

```
cd /home/ma-user/work
mkdir MindFormers
cd MindFormers
conda activate mindspore_py39
```

切换后需使用如下命令进行环境配置和相关支持库升级

```
export LD_PRELOAD=/home/ma-user/anaconda3/envs/MindSpore/lib/python3.9/site-packages/torch.libs/libgomp-d22c30c5.so.1.0.0
pip install langchain
pip install sentence_transformers
pip install unstructured
pip install --upgrade lxml
pip -vvv install --upgrade --force-reinstall cffi
pip install faiss-cpu
pip install --upgrade pandas
```

### 2.1 mindformers安装

在运行路径下，拉取最新的mindformers源码并安装

```
git clone -b dev https://gitee.com/mindspore/mindformers.git
cd mindformers
bash build.sh
cd ..
```

### 2.2 运行所需权重获取

代码通过Ernie1.0模型进行知识库的向量化以及后续输入的向量匹配，需通过互联网自行获取权重（https://baidu-nlp.bj.bcebos.com/ERNIE_stable-1.0.1.tar.gz）。

下载并解压缩后，可使用`utils/model_conver.py`进行权重转化，使用方式

```
python utils/model_conver.py --input_path xxxx -output_path xxx
```

通过互联网获取的Ernie模型的词向量文件中，对字符进行了编号，须通过`utils/vocab_conver.py`进行更新，使用方式

```
python utils/vocab_conver.py
```

注意： 权重迁移需要同时安装MindSpore和Paddle，由于Paddle不支持Arm环境，本步骤需要在x86环境下运行。权重迁移仅需要两个框架的CPU版本即可完成，可本地完成后上传转换后的Checkpoint使用。

本地大语言模型则通过mindformers实现的chatglm-6b进行支持，首次运行代码时会自动拉取权重。



## 3. 基于LangChain的大预言模型问答

### 3.1 mindformers本地模型与LangChain框架组合使用

可以使用如下命令直接调用模型进行问答使用。

```
python glmwithlangchain.py
```

### 3.2 通过LangChain框架管理向量库并基于向量库对mindformers本地模型问答进行优化

可根据自身需求进行知识库创建，可支持.txt，.md等格式的文件，可将需要加载的文件路径配置于chatbot.py文件的file变量中。

预先获取的ernie权重需将对应的ckpt文件路径配置于chatbot.py文件的embeding_model变量中。

预先获取的ernie词向量文件需将对应的ckpt文件路径配置于chatbot.py文件的embeding_tokenizer变量中。

可以使用如下命令直接调用模型进行问答使用。

```
python chatbot.py
```

未完待续....
